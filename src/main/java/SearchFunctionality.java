import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SearchFunctionality {
    List<String> cityNames = Arrays.asList("Paris", "Budapest", "Skopje", "Rotterdam", "Valencia", "Vancouver", "Amsterdam", "Vienna", "Sydney", "New York City", "London", "Bangkok", "Hong Kong", "Dubai", "Rome", "Istanbul");

    public List<String> citySearch(String city) {
        if (city == "*") return cityNames.stream().collect(Collectors.toList());
        else if (city.length() < 2) return null;
        else {
            return cityNames.stream()
                    .filter(f -> f.contains(city))
                    .map(m -> m.toString())
                    .collect(Collectors.toList());
        }
    }

}
