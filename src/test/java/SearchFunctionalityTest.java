import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SearchFunctionalityTest {
    private SearchFunctionality tdd;

    @BeforeEach
    void setUp() {
        tdd = new SearchFunctionality();
    }

    @Test
    void shouldReturnNoResultForStringLessThanTwo() {
        //Given
        String keyWord = "P";
//When
        List<String> returnedCityNames = tdd.citySearch(keyWord);
//Then
        assertEquals(null, returnedCityNames);
    }

    @Test
    void shouldReturnResultsForStringMoreThanOneChar() {
        //Given
        String keyWord = "Va";
//When
        List<String> returnedCityNames = tdd.citySearch(keyWord);
//Then
        List<String> cityNames = Arrays.asList("Valencia", "Vancouver");
        assertEquals(cityNames, returnedCityNames);
    }

    @Test
    void shouldReturnAllCities() {
//Given
        String keyWord = "*";
//When
        List<String> returnedCityNames = tdd.citySearch(keyWord);

//Then
        List<String> cityNames = Arrays.asList("Paris", "Budapest", "Skopje", "Rotterdam", "Valencia", "Vancouver", "Amsterdam", "Vienna", "Sydney", "New York City", "London", "Bangkok", "Hong Kong", "Dubai", "Rome", "Istanbul");
        assertEquals(cityNames, returnedCityNames);
    }

    @Test
    void shouldReturnPartialSearchResult() {
//Given
        String keyWord = "dam";
//When
        List<String> returnedCityNames = tdd.citySearch(keyWord);
//Then
        List<String> cityNames = Arrays.asList("Rotterdam", "Amsterdam");
        assertEquals(cityNames, returnedCityNames);
    }

    @Test
    void shouldNotReturnResultForCaseSensetiveIssues() {
        //Given
        String keyWord = "va";
//When
        List<String> returnedCityNames = tdd.citySearch(keyWord);
//Then
        List<String> nullCityNames = new ArrayList<>();
        assertEquals(nullCityNames, returnedCityNames);
    }


}
